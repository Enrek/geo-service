# GEO Service

## Requirements
* Java 10
* MySQL 8

## Database
To initialize database execute **init.sql** script. It will create an empty database with the required table. Another part of the database will be created by Hibernate.

## Run
To start application:
```
./gradlew bootRun
```

## API
* *GET /vehicles* - to reach REST HATEOAS main endpoint
* *GET /vehicles/search?latitudeFirst=0&longitudeFirst=0&latitudeSecond=0&longitudeSecond=0* - to search in rectange
* *REST CRUD* - available for vehicle resources 


Search result example
```
{
"_embedded": {
"vehicles": [
{
"position": {
"latitude": 11.445,
"longitude": 12.213
},
"lastUpdateTimestamp": "2019-08-14T18:36:17",
"_links": {
"self": {
"href": "http://localhost:8080/vehicles/3"
},
"vehicle": {
"href": "http://localhost:8080/vehicles/3"
}
}
},
{
"position": {
"latitude": 11.445,
"longitude": 12.213
},
"lastUpdateTimestamp": "2019-08-14T18:36:16",
"_links": {
"self": {
"href": "http://localhost:8080/vehicles/2"
},
"vehicle": {
"href": "http://localhost:8080/vehicles/2"
}
}
}
]
},
"_links": {
"self": {
"href": "http://localhost:8080/vehicles/search?latitudeFirst=0&longitudeFirst=0&latitudeSecond=13&longitudeSecond=122&page=0&size=12"
}
},
"page": {
"size": 12,
"totalElements": 2,
"totalPages": 1,
"number": 0
}
}
```

## Notes
### Spatial SRID
The application works with GPS data and processes it in SRID coordinates. Respectively latitude coordinates should be in -90 to 90 range and longitude in -180 to 180 value range.
### Performance
There are two main strategies on how to handle expected high load:
* Vertical - in a case with powerful machines we can update connection pool parameters and tune-up MySQL
* Horizontal - an increasing number of application and database replicas (require MySQL Cluster) with load-balance will provide enough bandwidth for this service