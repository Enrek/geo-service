DROP DATABASE IF EXISTS geoservice_db;
CREATE DATABASE geoservice_db;
USE geoservice_db;
DROP TABlE IF EXISTS `vehicle` ;

CREATE TABLE `vehicle` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `last_update_timestamp` datetime NOT NULL,
  `position` point NOT NULL SRID 4326,
  SPATIAL INDEX(`position`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;