package com.dmytro.ovcharov.geoservice.repository;

import com.dmytro.ovcharov.geoservice.model.entity.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "vehicles")
public interface VehicleRepository extends JpaRepository<Vehicle, Integer> {

    @Query(value = "SELECT * from vehicle WHERE MBRWithin(position,ST_PolygonFromText(:polygon,4326))",
            countQuery = "SELECT count(*) from vehicle WHERE MBRWithin(position,ST_PolygonFromText(:polygon,4326))",
            nativeQuery = true)
    Page<Vehicle> find(@Param("polygon") String polygon, Pageable pageable);
}
