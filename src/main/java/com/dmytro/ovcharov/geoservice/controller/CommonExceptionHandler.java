package com.dmytro.ovcharov.geoservice.controller;

import com.dmytro.ovcharov.geoservice.exeption.LocationParseException;
import com.dmytro.ovcharov.geoservice.model.response.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestControllerAdvice
public class CommonExceptionHandler extends ResponseEntityExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(CommonExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleException(Exception ex) {
        logger.error("Internal server error", ex);
        return errorResponse(INTERNAL_SERVER_ERROR, ex.getMessage() == null ? "Unknown error" : ex.getMessage());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> handleConstraintViolationException(Exception ex) {
        logger.debug("Constraint violation error", ex);
        return errorResponse(BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler(LocationParseException.class)
    public ResponseEntity<?> handleLocationParseException(Exception ex) {
        logger.debug("Location parse error", ex);
        return errorResponse(BAD_REQUEST, ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex,
                                                        HttpHeaders headers,
                                                        HttpStatus status,
                                                        WebRequest request) {
        logger.debug("Type mismatch exception", ex);
        return (ResponseEntity<Object>) errorResponse(BAD_REQUEST, ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
                                                                          HttpHeaders headers,
                                                                          HttpStatus status,
                                                                          WebRequest request) {
        logger.debug("Missing servlet request parameter exception", ex);
        return (ResponseEntity<Object>) errorResponse(BAD_REQUEST, ex.getMessage());
    }


    private ResponseEntity<?> errorResponse(HttpStatus httpStatus, String description) {
        return ResponseEntity.status(httpStatus).body(new ErrorResponse(description));
    }
}
