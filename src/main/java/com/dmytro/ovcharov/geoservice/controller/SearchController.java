package com.dmytro.ovcharov.geoservice.controller;

import com.dmytro.ovcharov.geoservice.model.entity.Vehicle;
import com.dmytro.ovcharov.geoservice.repository.VehicleRepository;
import com.dmytro.ovcharov.geoservice.util.VehicleResourceAssembler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;


@RestController
@RepositoryRestController
@Validated
public class SearchController {

    private static final String POLYGON_WKT_TEMPLATE = "POLYGON((%.03f %.03f,%.03f %.03f,%.03f %.03f,%.03f %.03f,%.03f %.03f))";
    private Logger logger = LoggerFactory.getLogger(SearchController.class);

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private VehicleResourceAssembler vehicleResourceAssembler;

    @Autowired
    private PagedResourcesAssembler<Vehicle> vehiclePagedResourcesAssembler;

    @GetMapping("/vehicles/search")
    public ResponseEntity<?> search(@RequestParam("latitudeFirst") @Min(-90) @Max(90) Double latitudeFirst,
                                    @RequestParam("longitudeFirst") @Min(-180) @Max(180) Double longitudeFirst,
                                    @RequestParam("latitudeSecond") @Min(-90) @Max(90) Double latitudeSecond,
                                    @RequestParam("longitudeSecond") @Min(-180) @Max(180) Double longitudeSecond,
                                    Pageable pageable) {
        logger.info("Requesting vehicles in rectangle {},{} | {},{}", latitudeFirst, longitudeFirst, latitudeSecond, longitudeSecond);

        String polygonWKT = createPolygon(latitudeFirst, longitudeFirst, latitudeSecond, longitudeSecond);
        Page<Vehicle> searchResults = vehicleRepository.find(polygonWKT, pageable);
        return ResponseEntity.ok(vehiclePagedResourcesAssembler.toResource(searchResults, vehicleResourceAssembler));
    }

    private String createPolygon(Double latitudeFirst,
                                 Double longitudeFirst,
                                 Double latitudeSecond,
                                 Double longitudeSecond) {
        String wktPolygon = String.format(POLYGON_WKT_TEMPLATE,
                latitudeFirst,
                longitudeFirst,
                latitudeSecond,
                longitudeFirst,
                latitudeSecond,
                longitudeSecond,
                latitudeFirst,
                longitudeSecond,
                latitudeFirst,
                longitudeFirst);
        logger.debug("Requesting search in polygon {}", wktPolygon);
        return wktPolygon;
    }
}