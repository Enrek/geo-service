package com.dmytro.ovcharov.geoservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GEOServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GEOServiceApplication.class, args);
    }

}
