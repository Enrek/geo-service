package com.dmytro.ovcharov.geoservice.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.locationtech.jts.geom.Point;

import java.io.IOException;

public class PointSerializer extends StdSerializer<Point> {

    public PointSerializer() {
        this(null);
    }

    public PointSerializer(Class<Point> pointClass) {
        super(pointClass);
    }

    @Override
    public void serialize(Point value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
        jgen.writeStartObject();
        jgen.writeNumberField("latitude", value.getX());
        jgen.writeNumberField("longitude", value.getY());
        jgen.writeEndObject();
    }
}
