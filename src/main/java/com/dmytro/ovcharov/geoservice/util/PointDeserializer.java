package com.dmytro.ovcharov.geoservice.util;

import com.dmytro.ovcharov.geoservice.exeption.LocationParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;

import java.io.IOException;

public class PointDeserializer extends StdDeserializer<Point> {

    private static final String POINT_WKT_TEMPLATE = "POINT(%.03f %.03f)";
    private final WKTReader wktReader;

    public PointDeserializer() {
        this(null);
    }

    public PointDeserializer(Class<Point> pointClass) {
        super(pointClass);
        wktReader = new WKTReader();
    }

    @Override
    public Point deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        try {
            Double latitude = getDoubleFromNode(node, "latitude");
            Double longitude = getDoubleFromNode(node, "longitude");
            assertLatitudeRange(latitude);
            assertLongitudeChange(longitude);
            Point point = (Point) wktReader.read(String.format(POINT_WKT_TEMPLATE, latitude, longitude));
            point.setSRID(4326);
            return point;
        } catch (ParseException e) {
            throw new LocationParseException("Latitude/Longitude parameters cannot't be parsed as point", e);
        }
    }

    private void assertLongitudeChange(Double longitude) {
        if (longitude > 180 || longitude < -180)
            throw new LocationParseException("longitude should be in -180 to 180 value range");
    }

    private void assertLatitudeRange(Double latitude) {
        if (latitude > 90 || latitude < -90)
            throw new LocationParseException("latitude should be in -90 to 90 value range");
    }

    private Double getDoubleFromNode(JsonNode node, String fieldName) {
        JsonNode fieldNode = node.get(fieldName);
        if (fieldNode == null) {
            throw new LocationParseException(fieldName + " should be not null");
        }
        try {
            return Double.parseDouble(fieldNode.asText());
        } catch (NumberFormatException e) {
            throw new LocationParseException(fieldName + " should be right formatted double", e);
        }
    }
}
