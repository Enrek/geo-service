package com.dmytro.ovcharov.geoservice.util;

import com.dmytro.ovcharov.geoservice.model.entity.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class VehicleResourceAssembler implements ResourceAssembler<Vehicle, Resource<Vehicle>> {

    @Autowired
    private RepositoryEntityLinks entityLinks;

    @Override
    public Resource<Vehicle> toResource(Vehicle entity) {
        Link self = entityLinks.linkFor(Vehicle.class).slash(entity.getId()).withSelfRel();
        Link rel = entityLinks.linkFor(Vehicle.class).slash(entity.getId()).withRel("vehicle");
        return new Resource<>(entity, self, rel);
    }
}

