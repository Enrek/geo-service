package com.dmytro.ovcharov.geoservice.exeption;

public class LocationParseException extends IllegalArgumentException {
    public LocationParseException(String s) {
        super(s);
    }

    public LocationParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
