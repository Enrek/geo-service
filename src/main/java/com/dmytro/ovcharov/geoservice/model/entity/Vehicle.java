package com.dmytro.ovcharov.geoservice.model.entity;

import com.dmytro.ovcharov.geoservice.util.PointDeserializer;
import com.dmytro.ovcharov.geoservice.util.PointSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.locationtech.jts.geom.Point;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@RestResource
@Entity
@JsonComponent
@AllArgsConstructor
@NoArgsConstructor
public class Vehicle implements Serializable, Cloneable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    private Integer id;
    @Column(name = "position", columnDefinition = "POINT", nullable = false)
    @JsonSerialize(using = PointSerializer.class)
    @JsonDeserialize(using = PointDeserializer.class)
    private Point position;
    @Convert(converter = Jsr310JpaConverters.LocalDateTimeConverter.class)
    @Column(nullable = false)
    private LocalDateTime lastUpdateTimestamp = LocalDateTime.now();
}
