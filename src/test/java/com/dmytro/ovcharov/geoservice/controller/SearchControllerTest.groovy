package com.dmytro.ovcharov.geoservice.controller

import com.dmytro.ovcharov.geoservice.model.entity.Vehicle
import com.dmytro.ovcharov.geoservice.repository.VehicleRepository
import com.dmytro.ovcharov.geoservice.util.VehicleResourceAssembler
import org.locationtech.jts.geom.Point
import org.locationtech.jts.io.WKTReader
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.hateoas.Resource
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import java.time.LocalDateTime

import static org.hamcrest.collection.IsCollectionWithSize.hasSize
import static org.mockito.ArgumentMatchers.any
import static org.mockito.Mockito.when
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
class SearchControllerTest extends Specification {

    @MockBean
    VehicleRepository vehicleRepository

    @MockBean
    VehicleResourceAssembler vehicleResourceAssembler

    @Autowired
    MockMvc mvc

    def "search endpoint should return list of vehicles"() {
        given:
        def point = (Point) new WKTReader().read("POINT(2.2 10.2)")
        def vehicle = new Vehicle(3, point, LocalDateTime.now())
        when(vehicleResourceAssembler.toResource(any() as Vehicle))
                .thenReturn(new Resource(vehicle))
        when(vehicleRepository.find(Mockito.any() as String, Mockito.any() as Pageable))
                .thenReturn(new PageImpl<Vehicle>([vehicle]))

        when:
        def result = mvc.perform(
                get("/vehicles/search?latitudeFirst=0&longitudeFirst=0&longitudeSecond=5&latitudeSecond=10")
                        .accept(MediaType.APPLICATION_JSON_UTF8))

        then:
        result.andExpect(status().isOk())
                .andExpect(jsonPath('$._embedded.vehicles', hasSize(1)))
                .andExpect(jsonPath('$._embedded.vehicles[0].position.latitude').value(2.2))
                .andExpect(jsonPath('$._embedded.vehicles[0].position.longitude').value(10.2))
    }

}
