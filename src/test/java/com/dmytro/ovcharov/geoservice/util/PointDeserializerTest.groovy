package com.dmytro.ovcharov.geoservice.util

import com.dmytro.ovcharov.geoservice.exeption.LocationParseException
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import spock.lang.Specification
import spock.lang.Unroll

class PointDeserializerTest extends Specification {

    def jsonParser = Mock(JsonParser)
    def pointDeserializer = new PointDeserializer()
    def jsonNode = Mock(JsonNode)
    def objectCodec = Mock(ObjectCodec)

    void setup() {
        jsonParser.getCodec() >> objectCodec
        objectCodec.readTree(jsonParser) >> jsonNode
    }

    def "deserializer should produce JSON with proper fields"() {
        given:
        jsonNode.get("latitude") >> jsonNode
        jsonNode.get("longitude") >> jsonNode
        jsonNode.asText() >>> ["1.2", "4.2"]

        when:
        def result = pointDeserializer.deserialize(jsonParser, Mock(DeserializationContext))

        then:
        result.getX() == 1.2D
        result.getY() == 4.2D
        result.getSRID() == 4326
    }

    @Unroll
    def "deserializer should throw exception if required field #absentFieldName is absent"() {
        given:
        jsonNode.get(absentFieldName) >> null
        jsonNode.get(existFieldName) >> jsonNode
        jsonNode.asText() >> "1.2"
        when:
        pointDeserializer.deserialize(jsonParser, Mock(DeserializationContext))

        then:
        LocationParseException ex = thrown()
        ex.message == "$absentFieldName should be not null"

        where:
        absentFieldName | existFieldName
        "longitude"     | "latitude"
        "latitude"      | "longitude"
    }

    @Unroll
    def "deserializer should throw exception if required field #absentFieldName has wrong format"() {
        given:
        jsonNode.get(wrongValueFieldName) >> jsonNode
        jsonNode.get(rightValueFieldName) >> jsonNode
        jsonNode.asText() >>> [latitudeValue, longitudeValue]
        when:
        pointDeserializer.deserialize(jsonParser, Mock(DeserializationContext))

        then:
        LocationParseException ex = thrown()
        ex.message == "$wrongValueFieldName should be right formatted double"

        where:
        wrongValueFieldName | rightValueFieldName | latitudeValue | longitudeValue
        "longitude"         | "latitude"          | "1.2"         | "a1"
        "latitude"          | "longitude"         | "a1"          | "1.2"
    }

    @Unroll
    def "deserializer should throw exception `#message` if required in wrong range [#latitudeValue;#longitudeValue]"() {
        given:
        jsonNode.get("longitude") >> jsonNode
        jsonNode.get("latitude") >> jsonNode
        jsonNode.asText() >>> [latitudeValue, longitudeValue]
        when:
        pointDeserializer.deserialize(jsonParser, Mock(DeserializationContext))

        then:
        LocationParseException ex = thrown()
        ex.message == message

        where:
        latitudeValue | longitudeValue | message
        "1.2"         | "220"          | "longitude should be in -180 to 180 value range"
        "1.2"         | "-220"         | "longitude should be in -180 to 180 value range"
        "-91"         | "1.2"          | "latitude should be in -90 to 90 value range"
        "91"          | "1.2"          | "latitude should be in -90 to 90 value range"
    }
}
