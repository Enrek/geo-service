package com.dmytro.ovcharov.geoservice.util

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import org.locationtech.jts.geom.Point
import org.locationtech.jts.io.WKTReader
import spock.lang.Specification

class PointSerializerTest extends Specification {

    def pointSerializer = new PointSerializer()
    def jgen = Mock(JsonGenerator)

    def "serialize should produce JSON object with proper fields"() {
        given:
        def point = (Point) new WKTReader().read("POINT(2.2 10.2)")

        when:
        pointSerializer.serialize(point, jgen, Mock(SerializerProvider))

        then:
        with(jgen) {
            1 * writeFieldName("latitude")
            1 * writeNumber(2.2)
            1 * writeFieldName("longitude")
            1 * writeNumber(10.2)
        }
    }
}
